#Linux的发行版
Linux操作系统有众多发行版、为许多不同的目的而制作, 包括对不同计算机结构的支持, 对一个具体区域或语言的本地化，实时应用，和嵌入式系统，甚至许多版本故意地只加入免费软件。已经有超过三百个发行版被积极的开发，最普遍被使用的发行版有大约十二个。

这里只列举几个当前流行的Linux发行版。

##Debian
来源: 全球 Global

桌面: AfterStep, Blackbox, Fluxbox, **GNOME**, IceWM, **KDE**, LXDE, Openbox, WMaker, Xfce

类型: Desktop, Live Medium, Server

处理器架构: alpha, arm, armel, hppa, ia64, i386, m68k, mips, mipsel, powerpc, s390, sparc64, x86_64

基于何种发行: Independent

优点：非常稳定，卓越的质量控制，超过20,000数量的软件;比任何其他的Linux发行支持更多的处理器架构

缺点：保守 – 因为它的许多处理器架构的支持，最新的技术并不总是包括在内;周期缓慢（每1 – 3年发布稳定版），对开发人员邮件列表和博客的讨论有时是落后的。

软件包管理：高级包管理工具（**APT**）和 **DEB**包

可用的版本：面向11处理器架构的安装的CD / DVD光盘和live cd镜像，包括所有32位和64位Intel, AMD, Power和其他处理器

建议基于Debian的选择：MEPIS Linux, Ubuntu, sidux. Damn Small Linux (老式计算机), KNOPPIX (live CD), Dreamlinux (桌面版), Elive (desktop with Enlightenment), Xandros (commercial), 64 Studio (multimedia)

####官方网站
https://www.debian.org

##Ubuntu
来源: 马恩岛（位于英格兰岛和爱尔兰岛之间） Isle of Man

桌面: **GNOME**

类型: Beginners, **Desktop**, **Server**, Live Medium, Netbooks

处理器架构: i386, powerpc, sparc64, x86_64

基于何种发行: **Debian**

优点：固定的发布周期和支持期限;易于初学者学习;丰富的文档，包括官方和用户贡献的。

缺点：缺乏与Debian的兼容性

软件包管理：高级包管理工具（**APT**）的使用deb包

可用版本：Ubuntu，Kubuntu，Xubuntu上，Ubuntu Studio和 Mythbuntu 包含32位（i386）和64位（x86_64）处理器; Ubuntu Server edition版（包含SPARC处理器）

建议基于Ubuntu的选择： Linux Mint (桌面版), gOS (谷歌应用程序桌面版), OpenGEU (Enlightenemnt桌面版), Ultimate Edition (桌面版), CrunchBang Linux (Openbox桌面版), gNewSense (免费软件)

####官方网站
http://www.ubuntu.com

##Fedora
来源: 美国 USA

桌面: **GNOME**, **KDE**, LXDE, Openbox, Xfce

类型: **Desktop**, **Server**, Live Medium

处理器架构: i686, powerpc, x86_64

基于何种发行: Independent

优点：高度创新，出色的安全功能;数量众多的支持包，严格遵守自由软件

缺点：Fedora的优先目的往往偏向企业应用的特点，而不是桌面可用性

软件包管理：**YUM**和 **RPM**包管理

可用版本： Fedora(i386), 64-bit (x86_64) ,PowerPC (ppc) 处理器; Red Hat Enterprise Linux （i386）, IA64, PowerPC, s390x 和 x86_64 architectures; also live CD editions with GNOME 或 KDE

建议基于Fedora的选择： BLAG Linux 和 GNU (桌面版, 自由软件), Berry Linux (live CD), Yellow Dog Linux (苹果PowerPC处理器系统)

建议基于Red Hat的选择：CentOS, Scientific Linux, StartCom Enterprise Linux

####官方网站
http://fedoraproject.org/

##CentOS
来源: USA

桌面: **GNOME, KDE**

类型: **Desktop**, Live Medium, **Server**

处理器架构: i386, powerpc, s390, s390x, x86_64

基于何种发行: Fedora, **Red Hat**

优点：非常行之有效的，稳定可靠;免费下载和使用;配备了5年的免费安全更新，及时发布和安全更新

缺点：缺乏最新的Linux技术，其发行时，大多数软件已经过时

软件包管理：**YUM**图形和命令行实用工具使用**RPM**包

可用的版本：live DVD和CD的 (GNOME)为i386和x86_64的处理器，旧版本（3.x和4.x）Alpha，IA64和IBM提供的Z -系列（s390，s390x）处理器。

其他红帽和CentOS的克隆为基础的发行版：Scientific Linux, SME Server, StartCom Enterprise Linux, Fermi Linux, Rocks Cluster Distribution, Oracle Enterprise Linux

####官方网站
http://www.centos.org/

##OpenSUSE
来源: 德国 Germany

桌面: Blackbox, GNOME, IceWM, KDE, WMaker, Xfce

类型: **Desktop**, **Server**, Live Medium

处理器架构: i586, x86_64

基于何种发行: Independent

优点：综合，直观的配置工具，大量的软件支持，优秀网站的架构和精美的文档库

缺点：Novell公司与微软在2006年11月的专利交易看似合法化了微软对Linux的知识产权，其桌面安装和图形工具是有时被视为“臃肿和缓慢”

软件包管理：YaST的图形和命令行实用工具和RPM包管理

可用版本：openSUSE的32位（i386）中，64位（x86_64的）和PowerPC（PPC）的处理器（也可安装的现场光盘版），而i586 系统的SUSE Linux企业级桌面/服务器，除ia64，PowerPC上，s390，s390x和x86_64架构

####官方网站
http://www.opensuse.org/

##FreeBSD
来源: 美国USA

桌面: AfterStep, Blackbox, Fluxbox, IceWM, KDE, Openbox, WMaker, Xfce

类型: **BSD**

处理器架构: alpha, i386, ia64, sparc64, pc98, powerpc, x86_64

基于何种发行: Independent

优点：快速，稳定，供应超过15,000的软件应用程序（或“ports”的安装）;非常好的文档

缺点：在硬件支持方面落后于Linux，商业应用有限;缺乏图形化配置工具

软件包管理：一个完整的命令行包管理 “ports” (**TBZ**)

可用的版本：安装光盘Alpha版，AMD64位，i386的Ia64文件，PC98和SPARC64处理器

建议基于FreeBSD的选择：PC-BSD (桌面版), DesktopBSD (桌面版)), FreeSBIE (live CD)

其他BSD的选择：OpenBSD, NetBSD, DragonFly BSD, MidnightBSD

####官方网站
http://www.freebsd.org/

*Note：关于RedHat，本身可以免费下载，但是如果要用官方的更新或者技术支持才要钱。许多应用与生产环境的RedHat发行版都要付费。*

* * *
**由Linux开发小组提供，QQ Group：234144486**
