#Markdown
##简介

是一种轻量级标记语言，创始人为约翰·格鲁伯（John Gruber）。

它允许人们“使用易读易写的纯文本格式编写文档，然后转换成有效的XHTML(或者HTML)文档”。

***Note:**许多开源项目的README都采用了Markdown语法，来展示简洁、统一、美观的说明文档。许多网站也集成有Markdown的解析模块，来展示文章，高效，降低网络负载。本文件就是用Markdown语法编写的。*

##一些学习资料
* 维基百科 http://zh.wikipedia.org/wiki/Markdown
* 快速入门 http://wowubuntu.com/markdown/basic.html
* 完整语法说明 http://wowubuntu.com/markdown/index.html

##实现版本
* [Sundown](https://github.com/vmg/sundown) 一个用C写的Markdown实现
* [MarkdownJ](http://code.google.com/p/markdownj/) 纯Java实现
* [markdown.lua](http://luaforge.net/projects/markdown/) Lua实现
* [PHP Markdown](http://michelf.com/projects/php-markdown/) PHP实现

##前端解析模块
* [markdown-js](https://github.com/evilstreak/markdown-js/)

##编辑器/查看器
* [MarkdownPad](http://markdownpad.com/) Full-featured Markdown editor for Windows.

	*Dependence：Microsoft .NET Framework 4.0*
* [ReText](http://sourceforge.net/p/retext/home/ReText/) For Linux
* [Mou](http://www.appinn.com/mou/) For Mac
* [Dilinger](http://dillinger.io/) For Web

	*Note：一下强大的文本编辑器也支持它，例如EverEdit，Sublime Text等*
* * *
**由Linux开发小组提供，QQ Group：234144486**